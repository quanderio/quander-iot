var Server = require('socket.io');
var PORT   = 3030;
var server = require('http').Server();

var io = Server(PORT);

io.close(); // Close current server

server.listen(PORT); // PORT is free to use

io = Server(server);

setInterval(function () {
  io.sockets.emit('event', {
    name: "RECORDING_STARTED",
    payload: {
      remaining: 10.00,
      scene: {
        name: "Quander News (Desk)[Video]",
        id: "QNDV",
        type: "VIDEO"
      }
    }
  });
  setTimeout(function () {
    io.sockets.emit('event', {
      name: "RECORDING_FINISHED",
      payload: {
        remaining: 0.00,
        scene: {
          name: "Quander News (Desk)[Video]",
          id: "QNDV",
          type: "VIDEO"
        }
      }
    });
  }, 5000);
  setTimeout(function () {
    io.sockets.emit('event', {
      name: "EXPERIENCE_STARTED",
      payload: {
        scene: {
          name: "Quander News (Desk)[Video]",
          id: "QNDV",
          type: "VIDEO"
        }
      }
    });
  }, 1000);
}, 10000);
