const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const gpio = require('rpi-gpio');
const Relay = require('./relay');

const port = process.env.PORT || 3000;

server.listen(port, function () {
  console.log('Server listening at port %d', port);
});

app.use(express.static(__dirname + '/public'));

let serverURL = null;

io.on('connection', function (socket) {
  socket.on('new message', function (data) {
    switch(data) {
      case "disconnect":
        serverURL = null;
        io.sockets.emit('disconnect', {});

        break;
      default:
    }
  });

  socket.on('add user', function (url) {
    socket.emit('login', {
      username: serverURL ? serverURL : url
    });

    if (serverURL) return;

    serverURL = url;

    const relay = new Relay(11);

    relay.init().then(() => {
      console.log("Setup Done");

      const socketClient = require('socket.io-client')(serverURL);

      socketClient.on('event', function(data) {
        io.sockets.emit('new message', {
          username: serverURL,
          message: data.name
        });

        console.log(data);

        switch(data.name) {
          case "RECORDING_STARTED":
            relay.start().then(() => {
              io.sockets.emit('new message', {
                username: 'Device',
                message: 'Relay on'
              });
            });
            break;
          case "RECORDING_FINISHED":
            relay.stop().then(() => {
              io.sockets.emit('new message', {
                username: 'Device',
                message: 'Relay off'
              });
            });
            break;
          default:
            io.sockets.emit('new message', {
              username: 'Device',
              message: 'Action Unrecognized'
            });
        }
      });
    });
  });

  socket.on('disconnect', function () {

  });
});

