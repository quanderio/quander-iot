const gpio = require('rpi-gpio');

/**
 * @class
 */
class Relay {
  constructor(pin) {
    this.pin = pin;
  }

  init() {
    return new Promise((resolve, reject) => {
      gpio.setup(this.pin, gpio.DIR_OUT, (err) => {
        if (err) reject(err);

        gpio.write(this.pin, false, () => {
          resolve();
        });
      });
    });
  }

  start(timeout) {
    return new Promise((resolve, reject) => {
      gpio.write(this.pin, true, () => {
        resolve();
      });
    });
  }

  stop(timeout) {
    return new Promise((resolve, reject) => {
      gpio.write(this.pin, false, () => {
        resolve();
      });
    });
  }
}

module.exports = Relay;